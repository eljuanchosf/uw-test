package controllers

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/eljuanchosf/uw-test/internal/models/bill"
)

func GetBills() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		paramCustomerId := c.Query("customer_id")
		paramMonth := c.Query("month")
		paramYear := c.Query("year")
		defer cancel()

		if paramCustomerId == "" {
			c.String(http.StatusBadRequest, "customer_id should be a valid UUID string")
			return
		}

		month, err := strconv.Atoi(paramMonth)
		if err != nil {
			c.String(http.StatusBadRequest, "month needs to be an integer")
			return
		}
		year, err := strconv.Atoi(paramYear)
		if err != nil {
			c.String(http.StatusBadRequest, "year needs to be an integer")
			return
		}

		customerBill, err := bill.GetByCustomerAndPeriod(ctx, paramCustomerId, month, year)
		if err != nil {
			switch err {
			case bill.ErrorNotFound:
				c.String(http.StatusNotFound, err.Error())
			default:
				c.String(http.StatusInternalServerError, err.Error())
			}
			return
		}
		err = customerBill.CalculateTotal()
		if err != nil {
			c.String(http.StatusInternalServerError, err.Error())
		}

		c.JSON(200, customerBill)
	}
}
