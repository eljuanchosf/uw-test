package db

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/eljuanchosf/uw-test/pkg/env"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const DatabaseName = "api"

func ConnectDB() *mongo.Client {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	databasePort := env.KeyOrDefault("MONGO_PORT", "27100")
	url := fmt.Sprintf("mongodb://localhost:%s", databasePort)
	client, err := mongo.NewClient(options.Client().ApplyURI(url))
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return client
}

var Client = ConnectDB()

func GetCollection(client *mongo.Client, collectionName string) *mongo.Collection {
	collection := client.Database(DatabaseName).Collection(collectionName)
	return collection
}

func DropDatabase(client *mongo.Client) bool {
	err := client.Database(DatabaseName).Drop(context.Background())
	return err == nil
}
