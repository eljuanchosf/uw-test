package bill

import (
	"context"
	"encoding/json"
	"errors"

	"github.com/bojanz/currency"
	"gitlab.com/eljuanchosf/uw-test/internal/models/product"

	"gitlab.com/eljuanchosf/uw-test/internal/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const GBP = "GBP"

type Bill struct {
	ID         primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	BillID     string             `json:"bill_id" bson:"bill_id"`
	Month      json.Number        `json:"month" bson:"month"`
	Year       json.Number        `json:"year" bson:"year"`
	CustomerID string             `json:"customer_id" bson:"customer_id"`
	TotalCost  string             `json:"total_cost" bson:"total_cost"`
	Products   []product.Product  `json:"products" bson:"products,omitempty"`
}

var (
	billsCollection = db.GetCollection(db.Client, "bills")
	ErrorNotFound   = errors.New("bill not found")
)

func New() *Bill {
	bill := Bill{}
	return &bill
}

func GetByCustomerAndPeriod(ctx context.Context, customerId string, month, year int) (*Bill, error) {
	bill := New()
	query := bson.M{"customer_id": customerId, "month": month, "year": year}
	result := billsCollection.FindOne(ctx, query)
	if result.Err() != nil {
		return nil, ErrorNotFound
	}
	err := result.Decode(bill)
	if err != nil {
		return nil, err
	}
	return bill, nil
}

func (b *Bill) Save() error {
	if _, err := billsCollection.InsertOne(context.Background(), b); err != nil {
		return err
	}
	return nil
}

func (b *Bill) CalculateTotal() error {
	locale := currency.NewLocale("uk")
	formatter := currency.NewFormatter(locale)
	totalCost, err := currency.NewAmount("0.00", GBP)
	if err != nil {
		return err
	}
	for _, p := range b.Products {
		productCost, err := formatter.Parse(p.TotalCost, GBP)
		if err != nil {
			return err
		}
		totalCost, err = totalCost.Add(productCost)
		if err != nil {
			return err
		}
	}
	b.TotalCost = totalCost.Number()
	return nil
}
