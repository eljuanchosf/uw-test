package docker

import (
	"context"
	"fmt"
	"io"
	"strconv"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"github.com/sirupsen/logrus"
)

func CreateNewContainer(repository, image, version string, hostPort, containerPort int) error {
	containerName := fmt.Sprintf("api-test-%s", image)
	logrus.SetLevel(logrus.DebugLevel)
	logger := logrus.WithField("container_name", containerName)
	cli, err := client.NewClientWithOpts()
	if err != nil {
		fmt.Println("Unable to create docker client")
		panic(err)
	}

	containerRunning, containerID, err := isContainerRunning(cli, containerName)
	if err != nil {
		return err
	}
	if containerRunning {
		logger.Debugf("reusing container: %s", containerID)
		return err
	}

	err = pullImage(cli, repository, image, version)
	if err != nil {
		return err
	}

	logger.Debug("starting container")
	hostBinding := nat.PortBinding{
		HostIP:   "0.0.0.0",
		HostPort: strconv.Itoa(hostPort),
	}
	containerNat, err := nat.NewPort("tcp", strconv.Itoa(containerPort))
	if err != nil {
		panic("Unable to get the port")
	}
	portBinding := nat.PortMap{containerNat: []nat.PortBinding{hostBinding}}

	ctx := context.Background()
	containerConfig := &container.Config{
		Image: fmt.Sprintf("%s:%s", image, version),
	}
	containerHostConfig := &container.HostConfig{
		PortBindings: portBinding,
	}

	networkConfig := &network.NetworkingConfig{}

	platform := &v1.Platform{
		Architecture: "amd64",
		OS:           "linux",
	}

	cont, err := cli.ContainerCreate(
		ctx,
		containerConfig,
		containerHostConfig,
		networkConfig,
		platform,
		containerName)
	if err != nil {
		panic(err)
	}

	err = cli.ContainerStart(context.Background(), cont.ID, types.ContainerStartOptions{})
	if err != nil {
		panic(err)
	}
	logger.Debugf("container started: %s", cont.ID)
	return nil
}

func pullImage(cli *client.Client, repository, image, version string) error {
	ctx := context.Background()

	if repository == "" {
		repository = "docker.io/library"
	}

	logger := logrus.WithFields(map[string]interface{}{
		"repository": repository,
		"image":      image,
		"version":    version,
	})

	imageListOptions := types.ImageListOptions{}

	imageList, err := cli.ImageList(ctx, imageListOptions)
	if err != nil {
		logger.Errorf("error getting Docker image list: %s", err)
	}
	for _, i := range imageList {
		for _, tag := range i.RepoTags {
			if tag == fmt.Sprintf("%s:%s", image, version) {
				logger.Debug("image already downloaded")
				return nil
			}
		}
	}

	logger.Info("pulling image")
	imageUrl := fmt.Sprintf("%s/%s:%s", repository, image, version)
	r, err := cli.ImagePull(ctx, imageUrl, types.ImagePullOptions{})
	if err != nil {
		logger.Errorf("error pulling image: %s", err)
		return err
	}

	_, err = io.Copy(devNull(0), r)
	if err != nil {
		logger.Errorf("error writing image: %s", err)
		return err
	}
	logger.Info("image pulled successfully")
	return nil
}

func isContainerRunning(cli *client.Client, name string) (bool, string, error) {
	name = fmt.Sprintf("/%s", name)
	listOptions := types.ContainerListOptions{}
	containers, err := cli.ContainerList(context.Background(), listOptions)
	if err != nil {
		return false, "", err
	}
	for _, c := range containers {
		for _, n := range c.Names {
			if n == name {
				return true, c.ID, nil
			}
		}
	}
	return false, "", nil
}

type devNull int

func (devNull) Write(p []byte) (int, error) {
	return len(p), nil
}
