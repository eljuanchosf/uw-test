# UW Brief Test

> *Important*: let me know if you need this repo private. :)

## How to run

### Required

1. Docker installed and available
2. Golang 1.17 installed and accessible

### Running

* Tests: `make test-verbose`
* Server `make run`

> Take in consideration that the test suite will download the `mongo:5.0` image from Dockerhub if not present. This might take a while.