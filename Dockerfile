FROM golang:1.17-alpine

WORKDIR /go/src/app
ADD . /go/src/app
RUN apk update && \
    apk add git gcc libc-dev make
RUN make dep && \
    make build
