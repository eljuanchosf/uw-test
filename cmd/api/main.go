package main

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/eljuanchosf/uw-test/internal/db"
	"gitlab.com/eljuanchosf/uw-test/internal/routes"
	"gitlab.com/eljuanchosf/uw-test/pkg/env"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	serverAddress := env.KeyOrDefault("API_ADDRESS", "localhost:6000")
	defer cancel()

	router := gin.Default()
	routes.BillRoute(router)

	db.ConnectDB()
	defer db.Client.Disconnect(ctx)

	err := router.Run(serverAddress)
	if err != nil {
		panic(err)
	}
}
