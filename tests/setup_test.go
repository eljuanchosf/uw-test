package tests

import (
	"context"
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/eljuanchosf/uw-test/internal/db"
	"gitlab.com/eljuanchosf/uw-test/pkg/docker"
)

func TestMain(m *testing.M) {
	err := docker.CreateNewContainer("", "mongo", "5.0", 27100, 27017)
	if err != nil {
		panic(err)
	}
	result := m.Run()
	if !db.DropDatabase(db.Client) {
		logrus.Errorf("could not clean up the database")
	}
	logrus.Debug("database cleaned")
	err = db.Client.Disconnect(context.Background())
	if err != nil {
		logrus.Errorf("error disconnecting the database: %s", err)
	}
	os.Exit(result)
}

func setupTest() (context.Context, context.CancelFunc) {
	return context.WithCancel(context.Background())
}

func tearDownTest(cancel context.CancelFunc) {
	cancel()
}

func IntegrationTest(testFunc func(context.Context)) {
	ctx, cancel := setupTest()
	defer tearDownTest(cancel)
	testFunc(ctx)
}
