package product

type Tariff struct {
	Name string `json:"name" bson:"name"`
	Cost string `json:"cost" bson:"cost"`
}

type Attributes struct {
	Tariff          *Tariff          `json:"tariff,omitempty" bson:"tariff,omitempty"`
	Consumption     string           `json:"consumption,omitempty" bson:"consumption,omitempty"`
	WalletName      string           `json:"wallet_name,omitempty" bson:"walletName,omitempty"`
	CbcTransactions []CbcTransaction `json:"transactions,omitempty" bson:"cbcTransactions,omitempty"`
	TotalCbcs       string           `json:"total_cbcs,omitempty" bson:"totalCbcs,omitempty"`
}

type CbcTransaction struct {
	Timestamp string `json:"timestamp" bson:"timestamp"`
	Merchant  string `json:"merchant" bson:"merchant"`
	Cost      string `json:"cost" bson:"cost"`
	Cashback  string `json:"cashback" bson:"cashback"`
}

type Product struct {
	Name       string     `json:"name" bson:"name"`
	TotalCost  string     `json:"total_cost" bson:"total_cost"`
	Attributes Attributes `json:"attributes,omitempty" bson:"attributes,omitempty"`
}
