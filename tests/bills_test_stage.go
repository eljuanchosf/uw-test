package tests

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/eljuanchosf/uw-test/internal/db"
	"gitlab.com/eljuanchosf/uw-test/internal/models/bill"
	models2 "gitlab.com/eljuanchosf/uw-test/internal/models/product"
	"gitlab.com/eljuanchosf/uw-test/internal/routes"
	"gitlab.com/eljuanchosf/uw-test/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
)

type billsStage struct {
	t            *testing.T
	ctx          context.Context
	customerID   string
	dbClient     *mongo.Client
	createdBill  *bill.Bill
	router       *gin.Engine
	httpResponse *http.Response
}

func BillsStage(ctx context.Context, t *testing.T) (*billsStage, *billsStage, *billsStage) {
	stage := &billsStage{
		t:   t,
		ctx: ctx,
	}

	errors.Must(os.Setenv("GIN_MODE", "release"))

	stage.dbClient = db.ConnectDB()
	gin.SetMode(gin.ReleaseMode) // Prevents gin from being too verbose
	stage.router = gin.Default()
	routes.BillRoute(stage.router)

	return stage, stage, stage
}

func (s *billsStage) and() *billsStage {
	return s
}

func (s *billsStage) i_request_the_bill_for_the_customer() *billsStage {
	var err error
	ts := httptest.NewServer(s.router)
	defer ts.Close()

	reqURL := fmt.Sprintf("%s/bills?customer_id=%s&month=%s&year=%s",
		ts.URL,
		s.customerID,
		s.createdBill.Month.String(),
		s.createdBill.Year.String(),
	)
	s.httpResponse, err = http.Get(reqURL)
	require.NoError(s.t, err)
	return s
}

func (s *billsStage) i_request_the_bill_for_a_non_existing_customer() *billsStage {
	var err error
	ts := httptest.NewServer(s.router)
	defer ts.Close()

	reqURL := fmt.Sprintf("%s/bills?customer_id=%s&month=%s&year=%s",
		ts.URL,
		uuid.New().String(),
		s.createdBill.Month.String(),
		s.createdBill.Year.String(),
	)
	s.httpResponse, err = http.Get(reqURL)
	require.NoError(s.t, err)
	return s
}

func (s *billsStage) i_request_the_bill_with_bad_parameters() *billsStage {
	var err error
	ts := httptest.NewServer(s.router)
	defer ts.Close()

	reqURL := fmt.Sprintf("%s/bills?customer_id=%s&month=%s&year=%s",
		ts.URL,
		s.customerID,
		"",
		s.createdBill.Year.String(),
	)
	s.httpResponse, err = http.Get(reqURL)
	require.NoError(s.t, err)
	return s
}

func (s *billsStage) the_bill_is_successfully_retrieved() *billsStage {
	defer s.httpResponse.Body.Close()
	body, err := ioutil.ReadAll(s.httpResponse.Body)
	if err != nil {
		require.NoError(s.t, err)
	}
	retrievedBill := bill.New()
	err = json.Unmarshal(body, retrievedBill)
	require.NoError(s.t, err)
	assert.NotEmpty(s.t, retrievedBill.ID)
	assert.Equal(s.t, s.createdBill.BillID, retrievedBill.BillID)
	assert.Equal(s.t, "21.00", retrievedBill.TotalCost)
	return s
}

func (s *billsStage) a_valid_customer_id() *billsStage {
	s.customerID = uuid.New().String()
	return s
}

func (s *billsStage) a_valid_bill_generated() *billsStage {
	energyProduct := models2.Product{
		Name:      "electricity",
		TotalCost: "0.5",
		Attributes: models2.Attributes{
			Tariff: &models2.Tariff{
				Name: "Green Electricity 2020",
				Cost: "0.5",
			},
			Consumption: "1",
		},
	}

	cbcProduct := models2.Product{
		Name:      "cbc",
		TotalCost: "20.50",
		Attributes: models2.Attributes{
			WalletName: "John's wallet",
			CbcTransactions: []models2.CbcTransaction{
				{
					Timestamp: "1 Jan 2020 4:30 PM",
					Merchant:  "Amazon UK",
					Cost:      "20.50",
					Cashback:  "1.50",
				},
			},
			TotalCbcs: "20.50",
		},
	}

	newBill := bill.New()
	newBill.BillID = uuid.New().String()
	newBill.CustomerID = s.customerID
	newBill.Month = "1"
	newBill.Year = "2022"
	newBill.Products = append(newBill.Products, energyProduct)
	newBill.Products = append(newBill.Products, cbcProduct)

	err := newBill.Save()
	require.NoError(s.t, err)
	s.createdBill = newBill
	return s
}

func (s *billsStage) the_status_code_is(statusCode int) *billsStage {
	require.Equal(s.t, statusCode, s.httpResponse.StatusCode)
	return s
}
