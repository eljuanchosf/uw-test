package tests

import (
	"context"
	"net/http"
	"testing"
)

func TestACC_GetBill_Successfully(t *testing.T) {
	IntegrationTest(func(ctx context.Context) {
		given, when, then := BillsStage(ctx, t)

		given.
			a_valid_customer_id().and().
			a_valid_bill_generated()

		when.
			i_request_the_bill_for_the_customer()

		then.
			the_status_code_is(http.StatusOK).and().
			the_bill_is_successfully_retrieved()
	})
}

func TestACC_GetBill_NotFound(t *testing.T) {
	IntegrationTest(func(ctx context.Context) {
		given, when, then := BillsStage(ctx, t)

		given.
			a_valid_customer_id().and().
			a_valid_bill_generated()

		when.
			i_request_the_bill_for_a_non_existing_customer()

		then.
			the_status_code_is(http.StatusNotFound)
	})
}

func TestACC_GetBill_BadParameters(t *testing.T) {
	IntegrationTest(func(ctx context.Context) {
		given, when, then := BillsStage(ctx, t)

		given.
			a_valid_customer_id().and().
			a_valid_bill_generated()

		when.
			i_request_the_bill_with_bad_parameters()

		then.
			the_status_code_is(http.StatusBadRequest)
	})
}
