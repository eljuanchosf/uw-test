package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/eljuanchosf/uw-test/internal/controllers"
)

func BillRoute(router *gin.Engine) {
	router.GET("/bills", controllers.GetBills())
}
